import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class SuperTest {

    @Test
    public void myFirstTest(){
        System.setProperty("webdriver.chrome.driver", "G:\\qa_java\\uacomhillel\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://kharkiv.ithillel.ua/");
        driver.findElement(By.xpath("//a[@class='g_button button_more' and contains(text(),\"Подробнее\")]")).click();
        assertTrue(driver.findElement(By.xpath(".//*[@class='wrap_nums mode_dtp state_shown1 state_shown']")).isDisplayed());
        driver.close();
    }

    @Test
    public void subscribe(){
        System.setProperty("webdriver.chrome.driver", "G:\\qa_java\\uacomhillel\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://kharkiv.ithillel.ua/");
        driver.findElement(By.xpath("//*[@class='input_mail g_input']")).click();
        driver.findElement(By.xpath("//*[@class='input_mail g_input']")).sendKeys("test@test.com");
        driver.findElement(By.xpath("//*[@class='button_subscribe g_button mode_small']")).click();
        WebElement dynamicElement = (new WebDriverWait(driver, 10)).until( ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='wrap_subscribed state_shown']")));
        assertTrue(driver.findElement(By.xpath("//*[@class='wrap_subscribed state_shown']")).isDisplayed());
        driver.close();
    }

    @Test
    public void formValidte(){
        System.setProperty("webdriver.chrome.driver", "G:\\qa_java\\uacomhillel\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://kharkiv.ithillel.ua/");
        driver.findElement(By.xpath("//*[@class='g_button button_enlist']")).click();
        driver.findElement(By.xpath("//*[@class='g_button button_register' and contains(text(),\"Оставить заявку\")]")).click();
        assertTrue(driver.findElement(By.xpath("//*[@class='show_registration state_modal']")).isDisplayed());
        driver.close();

    }

    @Test
    public void nonValidCertificate(){
        System.setProperty("webdriver.chrome.driver", "G:\\qa_java\\uacomhillel\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://kharkiv.ithillel.ua/");
        driver.findElement(By.xpath("//*[@class='wrap_cert']")).click();
        driver.findElement(By.xpath("//*[@class='g_button mode_small button_certNo']")).click();
        assertTrue(driver.findElement(By.xpath("//*[@class='text_notFound']")).isDisplayed());
        driver.close();
    }
    @Test
    public void loadMoreChecking(){
        System.setProperty("webdriver.chrome.driver", "G:\\qa_java\\uacomhillel\\src\\main\\resources\\chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get("https://kharkiv.ithillel.ua/");
        driver.findElement(By.xpath("//*[@class='item_mainMenu name_live']")).click();
        List<WebElement> forms = driver.findElements(By.className("entries__content"));
        int count = forms.size();
        //System.out.println(count);
        WebElement dynamicElement = (new WebDriverWait(driver, 10)).until( ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='entries_loadMore' and contains(text(),\"Еще вебинары\")]")));
        driver.findElement(By.xpath("//*[@id='entries_loadMore' and contains(text(),\"Еще вебинары\")]")).click();
        WebElement dynamicElement2 = (new WebDriverWait(driver, 10)).until( ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"entries_wrap\"]/li[9]/a/div[3]")));
        List<WebElement> forms2 = driver.findElements(By.className("entries__content"));
        int count2 = forms2.size();
        //System.out.println(count2);
        Assert.assertNotEquals(count,count2);
        driver.close();
    }


}

